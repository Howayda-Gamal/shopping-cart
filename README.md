1- Once the project is ready to run , just right click on project and choose 'run as' then 'Spring Boot App'

2- I am using h2 as an in-memory database and I created two files 'schema.sql' and 'data.sql' to create the table and insert some data into it, it will run automatically during spring boot app initialization.

3- After starting 'on port 8080' , go to 'http://localhost:8080/h2-console' and press connect to activate h2 database. make sure that the JDBC field has this value : 'jdbc:h2:mem:testdb' ,After connecting , then you can find the tables created and some records in it.

4- To perform the app ,, you can use first unit testing for Service that exist in test package.

5- For testing the rest api , please find the postman collection named "Shopping Cart.postman_collection.json" on the same level as this file in the project structure

6- The APIs start with /cart are accessible without authorization.

7- The APIs start with /admin needs admin authorization token first to access it.
to get Admin authorization you need to first register admin through /admin/register.
then authenticate admin by submitting the registered username and password to get the token.

References:
https://medium.com/@akhileshanand/spring-boot-api-security-with-jwt-and-role-based-authorization-fea1fd7c9e32

That's all for now , if you faced any unexpected error , contact me on howayda.gamal@gmail.com