package com.shopping.cart.demo.utils;

public class Constants {

    public static final String IN_PROGRESS_CART_STATUS = "IN_PROGRESS";

    public static final String CLOSED_CART_STATUS = "CLOSED";
}
