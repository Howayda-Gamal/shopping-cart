package com.shopping.cart.demo.dao;

import com.shopping.cart.demo.model.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryDAO extends CrudRepository<Category, Integer> {
}
