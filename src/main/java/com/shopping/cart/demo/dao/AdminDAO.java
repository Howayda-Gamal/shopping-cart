package com.shopping.cart.demo.dao;

import com.shopping.cart.demo.model.Admin;
import org.springframework.data.repository.CrudRepository;

public interface AdminDAO extends CrudRepository<Admin, Integer> {

    Admin findByUserName(String userName);
}
