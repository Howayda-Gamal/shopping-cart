package com.shopping.cart.demo.dao;

import com.shopping.cart.demo.model.CartItem;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Transient;
import java.util.List;

public interface OrderItemLineDAO extends CrudRepository<CartItem, Integer> {

    @Query(value = "SELECT * FROM order_line_item orderItem " +
            "INNER JOIN customer_order_order_line_item customerItem " +
            "ON orderItem.id = customerItem.order_line_items_id " +
            "WHERE customerItem.customer_order_id = :customerOrderId", nativeQuery = true)
    List<CartItem> getCartItemsByCustomerOrderId(int customerOrderId);

    @Query(value = "SELECT * FROM order_line_item orderItem " +
            "INNER JOIN customer_order_order_line_item customerItem " +
            "ON orderItem.id = customerItem.order_line_items_id " +
            "WHERE customerItem.customer_order_id = :customerOrderId " +
            "AND orderItem.product_id = :productId", nativeQuery = true)
    CartItem getCartItemsByCustomerOrderIdAndProductId(int customerOrderId, int productId);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM customer_order_order_line_item customerItem " +
            "WHERE customerItem.customer_order_id = :customerOrderId " +
            "AND customerItem.order_line_items_id = (SELECT orderItem.id FROM order_line_item orderItem " +
            "WHERE orderItem.product_id = :productId)", nativeQuery = true)
    void removeCartItemsByCustomerOrderIdAndProductId(int customerOrderId, int productId);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM customer_order_order_line_item customerItem " +
            "WHERE customerItem.customer_order_id = :customerOrderId", nativeQuery = true)
    void removeAllCartItemsByCustomerOrderId(int customerOrderId);
}
