package com.shopping.cart.demo.dao;

import com.shopping.cart.demo.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductDAO extends CrudRepository<Product,Integer> {

    @Query(value = "SELECT * FROM product p WHERE p.quantity >= 1", nativeQuery = true)
    List<Product> getAvailableProducts();

}
