package com.shopping.cart.demo.dao;

import com.shopping.cart.demo.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerDAO extends CrudRepository<Customer, Integer> {
}
