package com.shopping.cart.demo.dao;

import com.shopping.cart.demo.model.UserRole;
import org.springframework.data.repository.CrudRepository;

public interface UserRoleDAO extends CrudRepository<UserRole, String> {

}
