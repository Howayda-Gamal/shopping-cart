package com.shopping.cart.demo.dao;

import com.shopping.cart.demo.model.CartItem;
import com.shopping.cart.demo.model.CustomerOrder;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface CustomerOrderDAO extends CrudRepository<CustomerOrder, Integer> {

    @Query(value = "SELECT * FROM customer_order co WHERE co.customer_id = :customerId AND order_status = :status", nativeQuery = true)
    CustomerOrder getCustomerOrderByCustomerIdAndStatus(int customerId, String status);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO customer_order_order_line_item (customer_order_id, order_line_items_id) VALUES (:customerOrderId, :cartItemId)", nativeQuery = true)
    void addCartItemToCustomerOrder(int customerOrderId, int cartItemId);
}
