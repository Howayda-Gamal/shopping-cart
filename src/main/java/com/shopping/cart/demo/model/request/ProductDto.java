package com.shopping.cart.demo.model.request;

import com.shopping.cart.demo.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

    private int id;

    private String code;

    private String description;

    private int quantity;

    private byte[] image;

    private double price;

    private LocalDateTime lastUpdate = LocalDateTime.now();

    private int categoryId;

    public static final ProductDto map(Product product){
        ProductDto productDto = new ProductDto();
        productDto.setId(product.getId());
        productDto.setCode(product.getCode());
        productDto.setDescription(product.getDescription());
        productDto.setPrice(product.getPrice());
        productDto.setImage(product.getImage());
        productDto.setQuantity(product.getQuantity());
        productDto.setLastUpdate(product.getLastUpdate());
        productDto.setCategoryId(product.getCategory().getId());
        return productDto;
    }
}
