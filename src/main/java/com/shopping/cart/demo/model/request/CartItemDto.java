package com.shopping.cart.demo.model.request;

import com.shopping.cart.demo.model.CartItem;
import com.shopping.cart.demo.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartItemDto {

    private int productId;

    private int quantity;

    private double total;

    private int customerId;

    public static final CartItemDto map(CartItem cartItem){
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setProductId(cartItem.getProduct().getId());
        cartItemDto.setQuantity(cartItem.getQuantity());
        cartItemDto.setTotal(cartItem.getTotal());
        return cartItemDto;
    }
}