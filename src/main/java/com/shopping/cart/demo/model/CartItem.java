package com.shopping.cart.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "order_line_item")
public class CartItem {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JoinColumn(name = "product_id")
    @OneToOne(fetch = FetchType.LAZY)
    private Product product;

    @Column(name = "quantity")
    private int quantity;

    @Transient
    private double total;
}
