package com.shopping.cart.demo.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "admin_roles")
public class UserRole {

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_name")
    private Admin userName;

    @Id
    @Column(name = "role_name")
    private String roleName;
}
