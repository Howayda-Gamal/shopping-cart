package com.shopping.cart.demo.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "admin")
public class Admin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_name")
    private String userName;

    private String password;

    @OneToMany(mappedBy = "userName", fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<UserRole> userRoles;
}
