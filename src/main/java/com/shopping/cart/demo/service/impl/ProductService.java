package com.shopping.cart.demo.service.impl;

import com.shopping.cart.demo.dao.CategoryDAO;
import com.shopping.cart.demo.dao.ProductDAO;
import com.shopping.cart.demo.exception.ShoppingCartException;
import com.shopping.cart.demo.model.Category;
import com.shopping.cart.demo.model.Product;
import com.shopping.cart.demo.model.request.ProductDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ProductService {

    private final ProductDAO productDAO;

    private final CategoryDAO categoryDAO;

    public Product mapProduct(ProductDto productDto) throws ShoppingCartException {
        Optional<Category> category = categoryDAO.findById(productDto.getCategoryId());
        if(category.isPresent()) {
            Product product = new Product();
            product.setId(productDto.getId());
            product.setQuantity(productDto.getQuantity());
            product.setCategory(category.get());
            product.setCode(productDto.getCode());
            product.setDescription(productDto.getDescription());
            product.setImage(productDto.getImage());
            product.setPrice(productDto.getPrice());
            product.setLastUpdate(productDto.getLastUpdate());
            return product;
        }
        throw new ShoppingCartException("Category doesn't exist");
    }

    public Product addProduct(ProductDto productDto) throws ShoppingCartException {
        return productDAO.save(mapProduct(productDto));
    }

    public Product updateProduct(ProductDto productDto) throws ShoppingCartException {
        return productDAO.save(mapProduct(productDto));
    }

    public void deleteProduct(int productId) {
        productDAO.deleteById(productId);
    }
}
