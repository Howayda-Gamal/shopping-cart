package com.shopping.cart.demo.service.impl;

import com.shopping.cart.demo.dao.CustomerDAO;
import com.shopping.cart.demo.dao.CustomerOrderDAO;
import com.shopping.cart.demo.exception.ShoppingCartException;
import com.shopping.cart.demo.model.CartItem;
import com.shopping.cart.demo.model.Customer;
import com.shopping.cart.demo.model.CustomerOrder;
import com.shopping.cart.demo.utils.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CustomerOrderService {

    private final CustomerOrderDAO customerOrderDAO;

    private final CustomerDAO customerDAO;

    CustomerOrder getInProgressCustomerOrder(int customerId) {
        return customerOrderDAO.getCustomerOrderByCustomerIdAndStatus(customerId, Constants.IN_PROGRESS_CART_STATUS);
    }

    CustomerOrder createCustomerOrder(CartItem cartItem, int customerId) throws ShoppingCartException {
        Optional<Customer> customer = customerDAO.findById(customerId);
        if(customer.isPresent()) {
            CustomerOrder customerOrder = new CustomerOrder();
            customerOrder.setCustomerId(customer.get());
            customerOrder.setStatus(Constants.IN_PROGRESS_CART_STATUS);
            customerOrder.setTotalPrice(cartItem.getTotal());
            List<CartItem> itemLines = new ArrayList<>();
            itemLines.add(cartItem);
            customerOrder.setLineItems(itemLines);
            return customerOrderDAO.save(customerOrder);
        }
        throw new ShoppingCartException("Customer doesn't exist");
    }

    void addCartItemToCustomerOrder(int customerOrderId, int cartItemId) {
        customerOrderDAO.addCartItemToCustomerOrder(customerOrderId, cartItemId);
    }

    CustomerOrder updateCustomerOrder(CustomerOrder customerOrder, double newPrice) {
        customerOrder.setTotalPrice(customerOrder.getTotalPrice() + newPrice);
        return customerOrderDAO.save(customerOrder);
    }
}