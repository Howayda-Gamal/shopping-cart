package com.shopping.cart.demo.service.impl;

import com.shopping.cart.demo.dao.ProductDAO;
import com.shopping.cart.demo.exception.ShoppingCartException;
import com.shopping.cart.demo.model.CartItem;
import com.shopping.cart.demo.model.CustomerOrder;
import com.shopping.cart.demo.model.Product;
import com.shopping.cart.demo.model.request.CartItemDto;
import com.shopping.cart.demo.model.request.ProductDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
@RequiredArgsConstructor
public class CartService {

    private final CustomerOrderService customerOrderService;

    private final OrderItemLineService orderItemLineService;

    private final ProductDAO productDAO;

    public List<ProductDto> getAvailableProducts() {

        return productDAO.getAvailableProducts().stream().map(product -> ProductDto.map(product)).collect(Collectors.toList());
    }

    public void addProductToCart(CartItemDto cartItemDto) throws ShoppingCartException {
        CustomerOrder customerOrder = customerOrderService.getInProgressCustomerOrder(cartItemDto.getCustomerId());
        if (customerOrder != null) {
            customerOrderService.updateCustomerOrder(customerOrder, cartItemDto.getTotal());
            CartItem cartItem = orderItemLineService.createCartItem(orderItemLineService.mapCartItem(cartItemDto));
            customerOrderService.addCartItemToCustomerOrder(customerOrder.getId(), cartItem.getId());
        } else {
            customerOrder = customerOrderService.createCustomerOrder(orderItemLineService.mapCartItem(cartItemDto), cartItemDto.getCustomerId());
            CartItem cartItem = orderItemLineService.createCartItem(orderItemLineService.mapCartItem(cartItemDto));
            customerOrderService.addCartItemToCustomerOrder(customerOrder.getId(), cartItem.getId());
        }
    }

    public List<Product> getProductListByProductId(List<Integer> productIdList) {
        return (List<Product>) productDAO.findAllById(productIdList);
    }

    @SneakyThrows
    public List<CartItemDto> retrieveCartProducts(int customerId) {
        CustomerOrder customerOrder = customerOrderService.getInProgressCustomerOrder(customerId);
        if (customerOrder == null)
            throw new ShoppingCartException("Customer order not found");
        List<CartItem> cartItemList = orderItemLineService.getCartItemsByCustomerOrderId(customerOrder.getId());
//        List<Integer> productIdList = cartItemList.stream().flatMap(cart -> Stream.of(cart.getProduct().getId())).collect(Collectors.toList());
//        List<Product> productList = this.getProductListByProductId(productIdList);
        return cartItemList.stream().map(cartItem -> CartItemDto.map(cartItem)).collect(Collectors.toList());
    }
}
