package com.shopping.cart.demo.service.impl;

import com.shopping.cart.demo.dao.OrderItemLineDAO;
import com.shopping.cart.demo.dao.ProductDAO;
import com.shopping.cart.demo.exception.ShoppingCartException;
import com.shopping.cart.demo.model.CartItem;
import com.shopping.cart.demo.model.CustomerOrder;
import com.shopping.cart.demo.model.Product;
import com.shopping.cart.demo.model.request.CartItemDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderItemLineService {

    private final OrderItemLineDAO orderItemLineDAO;

    private final CustomerOrderService customerOrderService;

    private final ProductDAO productDAO;

    public CartItem createCartItem(CartItem cartItem){
        return orderItemLineDAO.save(cartItem);
    }

    public CartItem mapCartItem(CartItemDto cartItemDto) throws ShoppingCartException {
        Optional<Product> product = productDAO.findById(cartItemDto.getProductId());
        if(product.isPresent()) {
            CartItem cartItem = new CartItem();
            cartItem.setProduct(product.get());
            cartItem.setQuantity(cartItemDto.getQuantity());
            cartItem.setTotal(cartItemDto.getTotal());
            return cartItem;
        }
        throw  new ShoppingCartException("Product doesn't exist");
    }

    List<CartItem> getCartItemsByCustomerOrderId(int customerOrderId){
        return orderItemLineDAO.getCartItemsByCustomerOrderId(customerOrderId);
    }

    CartItem getCartItemsByCustomerOrderIdAndProductId(int customerOrderId, int productId){
        return orderItemLineDAO.getCartItemsByCustomerOrderIdAndProductId(customerOrderId, productId);
    }

    @SneakyThrows
    public void updateProductQuantity(int customerId, int productId, int quantity){
        CustomerOrder customerOrder = customerOrderService.getInProgressCustomerOrder(customerId);
        if(customerOrder == null)
            throw new ShoppingCartException("Customer order not found");
        CartItem cartItem = this.getCartItemsByCustomerOrderIdAndProductId(customerOrder.getId(),
                productId);
        cartItem.setQuantity(quantity);
        orderItemLineDAO.save(cartItem);
    }

    @SneakyThrows
    public void removeProductFromCart(int customerId, int productId){
        CustomerOrder customerOrder = customerOrderService.getInProgressCustomerOrder(customerId);
        if(customerOrder == null)
            throw new ShoppingCartException("Customer order not found");
        CartItem cartItem = this.getCartItemsByCustomerOrderIdAndProductId(customerOrder.getId(),
                productId);
        orderItemLineDAO.removeCartItemsByCustomerOrderIdAndProductId(customerOrder.getId(), cartItem.getProduct().getId());
        orderItemLineDAO.delete(cartItem);
    }

    @SneakyThrows
    public void emptyCart(int customerId){
        CustomerOrder customerOrder = customerOrderService.getInProgressCustomerOrder(customerId);
        if(customerOrder == null)
            throw new ShoppingCartException("Customer order not found");
        List<CartItem> cartItemList = this.getCartItemsByCustomerOrderId(customerOrder.getId());
        orderItemLineDAO.removeAllCartItemsByCustomerOrderId(customerOrder.getId());
        orderItemLineDAO.deleteAll(cartItemList);
    }
}
