package com.shopping.cart.demo.service.impl;

import com.shopping.cart.demo.dao.AdminDAO;
import com.shopping.cart.demo.dao.UserRoleDAO;
import com.shopping.cart.demo.model.Admin;
import com.shopping.cart.demo.model.UserRole;
import com.shopping.cart.demo.model.request.AdminDto;
import com.shopping.cart.demo.model.request.UserForm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@Service(value = "userService")
public class AdminService implements UserDetailsService {

    private final AdminDAO adminDAO;

    private final BCryptPasswordEncoder bcryptEncoder;

    private final UserRoleDAO userRoleDAO;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Admin admin = adminDAO.findByUserName(username);
        if (admin == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(admin.getUserName(), admin.getPassword(), getAuthority(admin));
    }

    private Set<SimpleGrantedAuthority> getAuthority(Admin admin) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        admin.getUserRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
        });
        return authorities;
    }

    public AdminDto save(UserForm userForm) {

        Admin admin = new Admin();
        admin.setUserName(userForm.getUserName());
        admin.setPassword(bcryptEncoder.encode(userForm.getPassword()));
        admin = adminDAO.save(admin);

        UserRole userRole = new UserRole();
        userRole.setRoleName("ADMIN");
        userRole.setUserName(admin);
        userRoleDAO.save(userRole);
        AdminDto adminDto = new AdminDto();
        adminDto.setUserName(admin.getUserName());
        return adminDto;
    }
}
