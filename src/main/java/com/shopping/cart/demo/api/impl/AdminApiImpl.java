package com.shopping.cart.demo.api.impl;

import com.shopping.cart.demo.api.AdminApi;
import com.shopping.cart.demo.config.TokenProvider;
import com.shopping.cart.demo.model.Admin;
import com.shopping.cart.demo.model.request.AdminDto;
import com.shopping.cart.demo.model.request.AuthToken;
import com.shopping.cart.demo.model.request.UserForm;
import com.shopping.cart.demo.model.request.ProductDto;
import com.shopping.cart.demo.service.impl.AdminService;
import com.shopping.cart.demo.service.impl.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class AdminApiImpl implements AdminApi {

    private final AuthenticationManager authenticationManager;

    private final TokenProvider jwtTokenUtil;

    private final ProductService productService;

    private final AdminService adminService;

    @Override
    public ResponseEntity addProduct(ProductDto productDto) {
        try {
            productService.addProduct(productDto);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity updateProduct(ProductDto productDto) {
        try {
            productService.updateProduct(productDto);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity deleteProduct(int productId) {
        try {
            productService.deleteProduct(productId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    public ResponseEntity<?> generateToken(@RequestBody UserForm userForm) throws AuthenticationException {

        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        userForm.getUserName(),
                        userForm.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = jwtTokenUtil.generateToken(authentication);
        return ResponseEntity.ok(new AuthToken(token));
    }

    public AdminDto saveUser(@RequestBody UserForm userForm) {
        return adminService.save(userForm);
    }
}
