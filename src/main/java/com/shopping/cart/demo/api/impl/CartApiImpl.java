package com.shopping.cart.demo.api.impl;

import com.shopping.cart.demo.api.CartApi;
import com.shopping.cart.demo.model.request.CartItemDto;
import com.shopping.cart.demo.model.request.ProductDto;
import com.shopping.cart.demo.service.impl.CartService;
import com.shopping.cart.demo.service.impl.OrderItemLineService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class CartApiImpl implements CartApi {

    private final CartService cartService;

    private final OrderItemLineService orderItemLineService;

    @Override
    public ResponseEntity<List<ProductDto>> getAvailableProducts() {
        return ResponseEntity.ok(cartService.getAvailableProducts());
    }

    @Override
    public ResponseEntity addProduct(CartItemDto cartItemDto) {
        try {
            cartService.addProductToCart(cartItemDto);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity<List<CartItemDto>> getProductsPerCart(int customerId) {
        return ResponseEntity.ok(cartService.retrieveCartProducts(customerId));
    }

    @Override
    public ResponseEntity updateProduct(int customerId, int productId, int quantity) {
        try {
            orderItemLineService.updateProductQuantity(customerId, productId, quantity);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity removeProductFromCart(int customerId, int productId) {
        try {
            orderItemLineService.removeProductFromCart(customerId, productId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public ResponseEntity emptyCart(int customerId) {
        try {
            orderItemLineService.emptyCart(customerId);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
