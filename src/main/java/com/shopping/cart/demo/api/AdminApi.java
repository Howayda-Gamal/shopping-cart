package com.shopping.cart.demo.api;

import com.shopping.cart.demo.model.Admin;
import com.shopping.cart.demo.model.request.AdminDto;
import com.shopping.cart.demo.model.request.UserForm;
import com.shopping.cart.demo.model.request.ProductDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/admin")
@CrossOrigin(origins = "*", maxAge = 3600)
public interface AdminApi {

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/addProduct")
    ResponseEntity addProduct(@RequestBody ProductDto productDto);

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(path = "/updateProduct")
    ResponseEntity updateProduct(@RequestBody ProductDto productDto);

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(path = "/deleteProduct")
    ResponseEntity deleteProduct(@RequestParam int productId);

    @RequestMapping(value = "/authenticateAdmin", method = RequestMethod.POST)
    public ResponseEntity<?> generateToken(@RequestBody UserForm userForm) throws AuthenticationException;

    @RequestMapping(value="/register", method = RequestMethod.POST)
    public AdminDto saveUser(@RequestBody UserForm userForm);
}
