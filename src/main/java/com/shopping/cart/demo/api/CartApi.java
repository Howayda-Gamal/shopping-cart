package com.shopping.cart.demo.api;

import com.shopping.cart.demo.model.request.CartItemDto;
import com.shopping.cart.demo.model.request.ProductDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/cart")
public interface CartApi {

    @GetMapping(path = "/")
    ResponseEntity<List<ProductDto>> getAvailableProducts();

    @PostMapping(path = "/addCartItem")
    ResponseEntity addProduct(@RequestBody CartItemDto cartItemDto);

    @GetMapping(path = "/getCartItems")
    ResponseEntity<List<CartItemDto>> getProductsPerCart(@RequestParam int customerId);

    @PutMapping(path = "/updateCartProduct")
    ResponseEntity updateProduct(@RequestParam int customerId, @RequestParam int productId, @RequestParam int quantity);

    @DeleteMapping(path = "/removeCartProduct")
    ResponseEntity removeProductFromCart(@RequestParam int customerId, @RequestParam int productId);

    @DeleteMapping(path = "/emptyCart")
    ResponseEntity emptyCart(@RequestParam int customerId);
}
