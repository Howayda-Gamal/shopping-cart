package com.shopping.cart.demo.exception;

public class ShoppingCartException extends Exception{

    public ShoppingCartException(String errorMsg){
        super(errorMsg);
    }
}
