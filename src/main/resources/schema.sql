create table sequence
(
   sequence_name varchar(50) not null,
   sequence_count decimal not null,
   primary key (sequence_name)
);

create table email_subscriber
(
    id int not null AUTO_INCREMENT,
    email varchar(255),
    first_name varchar(255),
    last_name varchar(255),
    primary key (id)
);

create table admin
(
    user_name varchar(15) not null,
    password varchar(255),
    primary key (user_name)
);

create table admin_roles
(
    user_name varchar(15),
    role_name varchar(15) not null,
    primary key (role_name),
    CONSTRAINT FK_Admin_Role FOREIGN KEY (user_name)
            REFERENCES admin(user_name)
);

create table category
(
    id bigint(20) not null AUTO_INCREMENT,
    last_update DATETIME,
    name varchar(255),
    primary key (id)
);

create table product
(
    id bigint(20) not null AUTO_INCREMENT,
    code varchar(255),
    description varchar(255),
    image LONGBLOB,
    last_update DATETIME,
    price double,
    quantity int(11),
    category_id bigint(20),
    primary key (id),
    CONSTRAINT FK_Product_Category FOREIGN KEY (category_id)
        REFERENCES category(id)
);

create table order_line_item
(
    id bigint(20) not null AUTO_INCREMENT,
    quantity int(11),
    product_id bigint(20),
    primary key (id),
    CONSTRAINT FK_Product_Order FOREIGN KEY (product_id)
        REFERENCES product(id)
);

create table customer
(
    id bigint(20) not null AUTO_INCREMENT,
    city varchar(255),
    email varchar(255),
    first_name varchar(255),
    last_name varchar(255),
    phone varchar(255),
    postal_code varchar(255),
    street varchar(255),
    primary key (id)
);

create table customer_order
(
    id bigint(20) not null AUTO_INCREMENT,
    order_date DATETIME,
    order_status varchar(255),
    order_amount double,
    customer_id bigint(20),
    primary key (id),
    CONSTRAINT FK_Customer_Order FOREIGN KEY (customer_id)
        REFERENCES customer(id)
);

create table payment
(
    id bigint(20) not null AUTO_INCREMENT,
    amount double,
    payment_date DATETIME,
    payment_status varchar(255),
    transaction_id varchar(255),
    customer_id bigint(20),
    customer_order_id bigint(20),
    primary key (id),
    CONSTRAINT FK_Customer_Payment FOREIGN KEY (customer_id)
        REFERENCES customer(id),
    CONSTRAINT FK_Customer_Payment_Order FOREIGN KEY (customer_order_id)
        REFERENCES customer_order(id)
);

create table customer_order_order_line_item
(
    customer_order_id bigint(20),
    order_line_items_id bigint(20),
    CONSTRAINT FK_Customer_Order_Item FOREIGN KEY (customer_order_id)
        REFERENCES customer_order(id),
    CONSTRAINT FK_Order_Line_Item FOREIGN KEY (order_line_items_id)
            REFERENCES order_line_item(id)
);
