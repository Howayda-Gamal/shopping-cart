insert into category (id, name) values (1, 'Apple watch series 6');
insert into product (id, code, description, price, quantity, category_id) values (1,'AW6BLUE','Apple watch 6', 1000.00, 5, 1);
insert into product (id, code, description, price, quantity, category_id) values (2,'AW6RED','Apple watch 6', 1000.00, 0, 1);
insert into customer (city, email, first_name, last_name, phone, postal_code, street) values ('Cairo', 'howayda.gamal@gmail.com', 'Howayda', 'Gamal', '01091674933', '0200', '100');
--insert into admin values ('Howayda', '123ABC123');
--insert into admin_roles values ('Howayda', 'ADMIN');

