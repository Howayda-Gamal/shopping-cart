package com.shopping.cart.demo.utils;

import com.shopping.cart.demo.model.CartItem;
import com.shopping.cart.demo.model.Customer;
import com.shopping.cart.demo.model.CustomerOrder;
import com.shopping.cart.demo.model.Product;
import com.shopping.cart.demo.model.request.CartItemDto;
import com.shopping.cart.demo.model.request.ProductDto;
import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

@Builder
public class GenerateStubs {

    public static List<ProductDto> generateAvailableProductsDto() {

        ProductDto productDto1 = new ProductDto();
        productDto1.setId(1);
        productDto1.setCode("AW6BLUE");
        productDto1.setDescription("Apple watch 6");
        productDto1.setPrice(1000.00);
        productDto1.setQuantity(5);
        productDto1.setCategoryId(1);

        ProductDto productDto2 = new ProductDto();
        productDto2.setId(2);
        productDto2.setCode("AW6RED");
        productDto2.setDescription("Apple watch 6");
        productDto2.setPrice(1000.00);
        productDto2.setQuantity(3);
        productDto2.setCategoryId(1);

        List<ProductDto> productList = new ArrayList<>();
        productList.add(productDto1);
        productList.add(productDto2);
        return productList;
    }

    public static List<Product> generateAvailableProducts() {

        Product product = new Product();
        product.setId(1);
        product.setCode("AW6BLUE");
        product.setDescription("Apple watch 6");
        product.setPrice(1000.00);
        product.setQuantity(5);

        Product product2 = new Product();
        product2.setId(2);
        product2.setCode("AW6RED");
        product2.setDescription("Apple watch 6");
        product2.setPrice(1000.00);
        product2.setQuantity(3);

        List<Product> productList = new ArrayList<>();
        productList.add(product);
        productList.add(product2);
        return productList;
    }

    public static CartItemDto generateCartItemRequest(){
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setProductId(1);
        cartItemDto.setQuantity(5);
        cartItemDto.setTotal(1200);
        cartItemDto.setCustomerId(1);
        return cartItemDto;
    }

    public static CustomerOrder generateCustomerOrder(){
        CustomerOrder customerOrder = new CustomerOrder();
        Customer customer = new Customer();
        customer.setId(1);
        customerOrder.setCustomerId(customer);
        customerOrder.setTotalPrice(1000);
        customerOrder.setStatus(Constants.IN_PROGRESS_CART_STATUS);
        return customerOrder;
    }

    public static CustomerOrder generateUpdatedCustomerOrder(CustomerOrder customerOrder, double total){
        customerOrder.setTotalPrice(customerOrder.getTotalPrice() + total);
        return customerOrder;
    }

    public static CartItem generateCartItem(){
        CartItem cartItem = new CartItem();
        Product product = new Product();
        product.setId(1);
        cartItem.setProduct(product);
        cartItem.setQuantity(5);
        cartItem.setTotal(1200);
        return cartItem;
    }

    public static CartItem mapCartItem(CartItemDto cartItemDto){
        CartItem cartItem = new CartItem();
        Product product = new Product();
        product.setId(cartItemDto.getProductId());
        cartItem.setProduct(product);
        cartItem.setQuantity(cartItemDto.getQuantity());
        cartItem.setTotal(cartItemDto.getTotal());
        return cartItem;
    }
}