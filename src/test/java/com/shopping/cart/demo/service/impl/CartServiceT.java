package com.shopping.cart.demo.service.impl;

import com.shopping.cart.demo.dao.ProductDAO;
import com.shopping.cart.demo.model.CartItem;
import com.shopping.cart.demo.model.CustomerOrder;
import com.shopping.cart.demo.model.request.CartItemDto;
import com.shopping.cart.demo.utils.GenerateStubs;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CartServiceT {

    @Mock
    ProductDAO productDAO;

    @Mock
    CustomerOrderService customerOrderService;

    @Mock
    OrderItemLineService orderItemLineService;

    @Test
    public void getAvailableProductsT() {

        Mockito.when(productDAO.getAvailableProducts())
                .thenReturn(GenerateStubs.generateAvailableProducts());
    }

    @Test
    public void addProductToCartWithExistingCustomerOrderSuccessT() {

        CartItemDto cartItemDto = GenerateStubs.generateCartItemRequest();
        CustomerOrder customerOrder = GenerateStubs.generateCustomerOrder();

        Mockito.when(customerOrderService.getInProgressCustomerOrder(cartItemDto.getCustomerId()))
                .thenReturn(customerOrder);
        Assertions.assertEquals(customerOrder.getId(), customerOrderService.getInProgressCustomerOrder(cartItemDto.getCustomerId()).getId());

        Mockito.when(customerOrderService.updateCustomerOrder(customerOrder, cartItemDto.getTotal()))
                .thenReturn(GenerateStubs.generateUpdatedCustomerOrder(customerOrder, cartItemDto.getTotal()));
        Assertions.assertEquals(customerOrder.getTotalPrice(), customerOrderService.updateCustomerOrder(customerOrder, cartItemDto.getTotal()).getTotalPrice());

        CartItem cartItem = GenerateStubs.mapCartItem(cartItemDto);
        Mockito.when(orderItemLineService.createCartItem(cartItem))
                .thenReturn(GenerateStubs.generateCartItem());
        Assertions.assertEquals(cartItem.getProduct().getId(), orderItemLineService.createCartItem(cartItem).getProduct().getId());
    }
}
